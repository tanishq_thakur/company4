//require('dotenv').config()

const express = require('express');
const bodyParser = require('body-parser');

// create express app
const app = express();

const jwt = require('jsonwebtoken')

app.use(express.json())
/*
const posts = [
    {
        username: 'Tanishq',
        password: 'don21'
    }
    ,{
        username: 'boss',
        password: 'hala'
    }
]
*/
// setup the server port
const port = process.env.PORT || 5000;

// parse request data content type application/x-www-form-rulencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse request data content type application/json
app.use(bodyParser.json());



// define root route
app.get('/', (req, res)=>{
    res.send('Hello World');
});
// import employee routes
const employeeRoutes = require('./src/routes/employee.route');

// import company routes
const companyRoutes = require('./src/routes/company.route');

// import adminlogin routes
const adminloginRoutes = require('./src/routes/adminlogin.route');

// import adminregister routes
const adminregisterRoutes = require('./src/routes/adminregister.route');

// import accessory_table routes
const accessory_tableRoutes = require('./src/routes/accessory_table.route');

// import addtask routes
const addtaskRoutes = require('./src/routes/addtask.route');

// import showtask routes
const showtaskRoutes = require('./src/routes/showtask.route');

// import manager routes
const managerRoutes = require('./src/routes/manager.route');

// import completed_task routes
const completed_taskRoutes = require('./src/routes/completed_task.route');

// import add_accessories routes
const add_accessoriesRoutes = require('./src/routes/add_accessories.route');


// create employee routes
app.use('/api/v1/employee', employeeRoutes);

// create company routes
app.use('/api/v1/company', companyRoutes);

// create adminlogin routes
app.use('/api/v1/adminlogin', adminloginRoutes);

// create adminregister routes
app.use('/api/v1/adminregister', adminregisterRoutes);

// create accessory_table routes
app.use('/api/v1/accessory_table', accessory_tableRoutes);

// create addtask routes
app.use('/api/v1/addtask', addtaskRoutes);

// create showtask routes
app.use('/api/v1/showtask', showtaskRoutes);

// create manager routes 
app.use('/api/v1/manager',managerRoutes);

// create completed_task routes 
app.use('/api/v1/completed_task',completed_taskRoutes);

// create add_accessories routes 
app.use('/api/v1/add_accessories',add_accessoriesRoutes);

app.get("/api", (req, res) => {
    res.json({
        message: "Hey there! welcome to api",
    });
});

app.post("/api/posts", verifyToken, (req, res) => {
    jwt.verify(req.token, "secretkey", (err, authData) => {
            if(err){
                res.sendStatus(403)  //forbidden
            }
            else{
              res.json({
                  message:"posts created..........",
                  authData,
             });
            }
          });      
});

app.post('/api/login', (req, res) => {
    const user ={
        id: 1,
        username: 'John',
        email: 'johny@gmail.com'
    }

    jwt.sign({user: user}, 'secretkey', (err,token) =>{
        res.json({
            token,
        });
    });
});

function verifyToken(req,res,next){
    const bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined'){
        const bearerToken =bearerHeader.split('')[1]
        req.token =bearerToken
        next()

    }else{
         res.sendStatus(403) //forbidden
    }
}

app.listen(3000, (req, res) => {
    console.log('server started on port 3000')
})

/*app.get('/posts', authenticateToken, (req, res) => {
    res.json(posts)
    //res.json(posts.filter(post => post.username === req.user.name))
})
*/
/*app.post('/login', (req, res) => {
    // Authenticae user

    const username = req.body.username
    const user = {name: username}

    const accessToken  =  jwt.sign(user,process.env.ACCESS_TOKEN_SECRET)
    res.json({ accessToken: accessToken })
})
*/
// listen to the port
//app.listen(port, ()=>{
  // console.log(`Express is running at port ${port}`);
/*
function authenticateToken(req, res, next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)

	jwt.veriy(token, pocess.env.ACCESS_TOKEN_SECRET, (err, user) => {
		if (err) return res.sendStatus(403)
		req.user = user
		next()
    })
}
*/
//app.listen(3000)
//});