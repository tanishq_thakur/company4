var dbConn  = require('../../config/db.config');

var add_accessoriess = function(add_accessoriess){
    this.add_accessories_empployee_id    =   add_accessoriess.add_accessories_empployee_id;
    this.add_accessories_empployee_name  =   add_accessoriess.add_accessories_empployee_name;
    this.add_accessories_accessory_name  =   add_accessoriess.add_accessories_accessory_name;
    this.add_accessories_accessory_quantity  =   add_accessoriess.add_accessories_accessory_quantity;
    this.add_accessories_accessory_status  =   add_accessoriess.add_accessories_accessory_status;
}

// get all add_accessoriess
add_accessoriess.getAlladd_accessoriess = (result) =>{
    dbConn.query('SELECT * FROM add_accessories', (err, res)=>{
        if(err){
            console.log('Error while fetching add_accessoriess', err);
            result(null,err);
        }else{
            console.log('add_accessoriess fetched successfully');
            result(null,res);
        }
    })
}

// get add_accessories by ID from DB
add_accessoriess.getadd_accessoriesByID = (id, result)=>{
    dbConn.query('SELECT * FROM add_accessoriess WHERE cId=?', id, (err, res)=>{
        if(err){
            console.log('Error while fetching add_accessoriess by id', err);
            result(null, err);
        }else{
            result(null, res);
        }
    })
}

// create new add_accessories
add_accessoriess.createadd_accessories = (add_accessoriesReqData, result) =>{
    dbConn.query('INSERT INTO add_accessoriess SET ? ', add_accessoriesReqData, (err, res)=>{
        if(err){
            console.log('Error while inserting data');
            result(null, err);
        }else{
            console.log('add_accessories created successfully');
            result(null, res)
        }
    })
}

// update add_accessories
add_accessoriess.updateadd_accessories = (id, add_accessoriesReqData, result)=>{
    dbConn.query("UPDATE add_accessories SET add_accessories_empployee_id=?,add_accessories_empployee_name=?,add_accessories_task_accessory_name=?,add_accessories_accessory_quantity=?,add_accessories_accessory_status  =? WHERE cId = ?", [add_accessoriesReqData.add_accessories_empployee_id,add_accessoriesReqData.add_accessories_empployee_name,add_accessoriesReqData.add_accessories_accessory_name,add_accessoriesReqData.add_accessories_accessory_quantity,add_accessoriesReqData.add_accessories_accessory_status, id], (err, res)=>{
        if(err){
            console.log('Error while updating the add_accessories');
            result(null, err);
        }else{
            console.log("add_accessories updated successfully");
            result(null, res);
        }
    });
}

// delete add_accessories
add_accessoriess.deleteadd_accessories = (id, result)=>{
    dbConn.query('DELETE FROM add_accessoriess WHERE cId=?', [id], (err, res)=>{
         if(err){
             console.log('Error while deleting the add_accessoriess');
             result(null, err);
         }else{
             result(null, res);
         }
     })
}

module.exports = add_accessoriess;