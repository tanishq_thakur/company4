var dbConn  = require('../../config/db.config');

var completed_tasks = function(completed_tasks){
    this.completed_task_empployee_id    =   completed_tasks.completed_task_empployee_id;
    this.completed_task_empployee_name  =   completed_tasks.completed_task_empployee_name;
    this.completed_task_task   =   completed_tasks.completed_task_task;
    this.completed_task_completion_status   =   completed_tasks.completed_task_completion_status ;
}

// get all completed_tasks
completed_tasks.getAllcompleted_tasks = (result) =>{
    dbConn.query('SELECT * FROM completed_task', (err, res)=>{
        if(err){
            console.log('Error while fetching completed_tasks', err);
            result(null,err);
        }else{
            console.log('completed_tasks fetched successfully');
            result(null,res);
        }
    })
}

// get completed_task by ID from DB
completed_tasks.getcompleted_taskByID = (id, result)=>{
    dbConn.query('SELECT * FROM completed_tasks WHERE cId=?', id, (err, res)=>{
        if(err){
            console.log('Error while fetching completed_tasks by id', err);
            result(null, err);
        }else{
            result(null, res);
        }
    })
}

// create new completed_task
completed_tasks.createcompleted_task = (completed_taskReqData, result) =>{
    dbConn.query('INSERT INTO completed_tasks SET ? ', completed_taskReqData, (err, res)=>{
        if(err){
            console.log('Error while inserting data');
            result(null, err);
        }else{
            console.log('completed_task created successfully');
            result(null, res)
        }
    })
}

// update completed_task
completed_tasks.updatecompleted_task = (id, completed_taskReqData, result)=>{
    dbConn.query("UPDATE completed_task SET completed_task_empployee_id=?,completed_task_empployee_name=?,completed_task_task=?,completed_task_completion_status  =? WHERE cId = ?", [completed_taskReqData.completed_task_empployee_id,completed_taskReqData.completed_task_empployee_name,completed_taskReqData.completed_task_task,completed_taskReqData.completed_task_completion_status , id], (err, res)=>{
        if(err){
            console.log('Error while updating the completed_task');
            result(null, err);
        }else{
            console.log("completed_task updated successfully");
            result(null, res);
        }
    });
}

// delete completed_task
completed_tasks.deletecompleted_task = (id, result)=>{
    dbConn.query('DELETE FROM completed_tasks WHERE cId=?', [id], (err, res)=>{
         if(err){
             console.log('Error while deleting the completed_tasks');
             result(null, err);
         }else{
             result(null, res);
         }
     })
}

module.exports = completed_tasks;